import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-calculo',
  templateUrl: './calculo.component.html',
  styleUrls: ['./calculo.component.css']
})
export class CalculoComponent implements OnInit {

  numero1:number= 25;
  numero2:number= 30;
  resultado:number;

  constructor() {
    this.resultado= this.numero1+this.numero2;

  }

  ngOnInit(): void {
  }

}
